#DIT IS BELANGRIJK: omdat het runnen van de webpage een infinite ding is moet dit script
#alleen gebruikt worden met subprocess.Popen anders wordt je script echt vol in de reet genomen

#imports
from tg import expose, TGController, AppConfig, request
from wsgiref.simple_server import make_server
import paramiko
import pprint
import os
dir_path = os.path.dirname(os.path.realpath(__file__))
#Controller

    #RootController
class RootController(TGController):
    @expose(dir_path+"/git.xhtml")
    def index(self, **kw):
        if request.method == 'POST':
            print("webhook")
        return

    @expose(dir_path+"/live_output.xhtml")
    def live_output(self, **kw):
        data = ""
        #if Git pull PC's
        if request.method == 'POST':
            """
            try:
                print(request.params['command'])
                print("SSH Git pull voor PC's runnen")
                #ssh dingen
                    #connecten
                ssh = paramiko.SSHClient()
                ssh.set_missing_host_key_policy(
                paramiko.AutoAddPolicy())
                ssh.connect('192.168.1.13', username='pi',
                password='raspberry')
                    #command runnen en data opslaan/printen
                stdin, stdout, stderr = ssh.exec_command(
                    #"git clone https://Tommie1337@bitbucket.org/jtjtesting/pythontesting.git ~/Desktop/GitRepo")
                    #"git reset --hard HEAD;git clean -xffd;
                #laatste versie van git repo downloaden
                "cd ~/Desktop/GitRepo;git pull")
                #output opslaan in variable
                data = stdout.readlines()
                print("Data1:")
                print(data)
            except:
                print("command not set")
            """
        return dict(data = data)


#AppConfig
config = AppConfig(minimal=True, root_controller=RootController())
config.renderers = ['kajiki']
config.serve_static = True
config.paths['static_files'] = "public"

application = config.make_wsgi_app()

#Server starten en shit
print("Serving on port 8080...")
httpd = make_server('', 8080, application)
httpd.serve_forever()
